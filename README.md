# M17 UF3 R1.1 Survivor 3D: Player i Càmera

## Recursos

## R1.1.1 Player Test: Moviment i animacions

Per realitzar aquesta part creareu una escena amb un terra, alguns obstacles, una càmera fixa i el nostre personatge.

L'escena tindrà nom de "Test-R1.1.1"

- 1 Importar un model de tipus humanoide que serà el protagonista del nostre joc.
    - 1.1 Fer servir models ja creats, per exemple el que s'esmenta a l'apartat de Recursos.
    - 1.2 Importar correctament materials i textures.
    - 1.3 Si no té el rigging fet recordeu el recurs de mixamo o blender.
- 2 Verbs que ha de tenir el personatge:
    - 2.1 Idle: repòs
    - 2.2 Caminar dret
    - 2.3 Córrer dret
    - 2.4 Disparar: El protagonista portarà una arma i ha de poder disparar mentre està quiet, caminant o saltant. No por disparar mentre va ajupit o roda cap endavant (extra).
        - 2.4.1 Afegir animation layers per afegir l'estat d'apuntar amb l'arma mentre camina. En aquest punt, treure i guardar la pistola es poden fer utilitzant transicions entre estats de l'Animator.
    - 2.5 Ajupir-se/caminar ajupit/aixecar-se: No pot disparar mentre es fan aquestes accions.
    - 2.6 Saltar: Saltarà sempre una mida fixa.
    - 2.7 Agafar / utilitzar: Animació genèrica que emprarem per interactuar amb elements de l'escena.  Exemples: obrir portes, agafar objectes, activar palanques.
    - 2.8 Celebració: Animació on es perd el control del personatge fins que acaba.
    - 2.9 (Extra) Idle avançat: El personatge realitza diferents animacions dependents del temps d'estat de repòs.
    - 2.10 (Extra) Rolling: Acció de tombarella cap endavant que es realitza mentres s'està ajupit. Es pot fer directament "rolling" quan s'està dret.
    - 2.11 (Extra) Saltar: Salt de mida variable segons script.
    - 2.12 (Extra) Caminar/Córrer ferit
    - 2.13 (Extra) Treure i guardar arma: Una animació feta adhoc de desenfundar i guardar l'arma. D'aquesta forma s'afina molt millor el verb de disparar.
- 3 Crear l'animator adient per a l'execució de les animacions dels verbs del personatge.
    - 3.1 Ha de tenir mínim 1 blend tree.
    - 3.2 Ha de tenir mínim 1 capa extra d'animació.
    - 3.3 (Extra) Blend tree 2D directional: Per fer el moviment del personatge.
- 4 Crear un script que controli el moviment del personatge.
    - 4.1 Aquest control ha de manegar les diferents animacions i capes.
    - 4.2 Ha de poder realitzar els verbs que s'esmenten anteriorment.
    - 4.3 Control del moviment del personatge.
        - 4.3.1 Emprar el Character Controller, i utilitzar la funció CharacterController.move()
        - 4.3.2 Emprar el Input Manager (el fet servit fins ara) amb el controls de teclat (WASD i fletxes) i ratolí direcció.
    - 4.4 Programar una tecla que executi l'animació de celebració.
    - 4.5 (Extra) Utilització de Input System per als controls del personatge.
    - 4.6 (Extra) Afegir un comandament tipus Xbox o PlayStation per a controlar el personatge.

## R1.1.2 Camera setting: Càmera i moments cinemàtics

Per realitzar aquesta segona part s'ha de crear una escena nova amb l'attrezzo igual a l'anterior escena i el personatge amb la càmara implementada. L'escena tindrà nom de "Test-R1.1.2".

- 1 Crear una càmera en 3a persona:
    - 1.1 Aquesta càmera ha d'estar creada amb l'eina cinemachine.
    - 1.2 La càmera rotarà sense passar a la part frontal del personatge.
    - 1.3 Quan el personatge porta l'arma desenfundada, la càmera estarà més a prop del personatge ajufant al jugador a apuntar millor.
        - 1.3.1 La càmera ha de tenir col·lisió amb els elements d'escena.
- 2 Moments cinemàtics:
    - 2.1 En prémer el botó de celebració, canvia la càmera a una altra càmera virtual en la qual es pugui veure aquesta animació.
        - 2.1.1 (Extra) Animar altres elements de l'escena: Afegir sistemes de partícules, animar altres elements, etc.
        -2.1.2 (Extra) Utilitza el PlayableDirector i Timeline per realitzar aquesta cinemàtica.
