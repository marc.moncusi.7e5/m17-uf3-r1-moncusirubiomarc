using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rake : MonoBehaviour
{
    int _rakeDamage = 1;
    [SerializeField] AudioSource _audioSource;

    private void OnTriggerEnter(Collider other)
    {
        IDamagable damagable = other.GetComponent<IDamagable>();
        if (damagable != null)
        {
            _audioSource.Play();
            damagable.ApplyDamage(_rakeDamage);
        }
    }

}
