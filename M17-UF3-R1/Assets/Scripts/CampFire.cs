using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampFire : MonoBehaviour
{
    int _enterDamage = 10;
    int _stayDamage = 5;
    bool _stayDamageTaken;
    float _stayDamageTimer = 2.5f;

    [SerializeField] AudioSource _audioSource;

    private void OnTriggerEnter(Collider other)
    {
        IDamagable damagable = other.GetComponent<IDamagable>();

        if (damagable != null)
        {
            _audioSource.Play();
            damagable.ApplyDamage(_enterDamage);
            StartCoroutine(WaitForDamage());
        }
    }

    private void OnTriggerStay(Collider other)
    {
        IDamagable damagable = other.GetComponent<IDamagable>();

        if (damagable != null && !_stayDamageTaken)
        {
            _audioSource.Play();
            damagable.ApplyDamage(_stayDamage);
            Debug.Log(_stayDamage);
            StartCoroutine(WaitForDamage());
        }
    }
    private IEnumerator WaitForDamage()
    {
        _stayDamageTaken = true;
        yield return new WaitForSeconds(_stayDamageTimer);//Wait for the transition
        _stayDamageTaken = false;
    }
}
