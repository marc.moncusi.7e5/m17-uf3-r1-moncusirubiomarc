using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{
    public static void LoadStartMenu() => SceneManager.LoadScene("Starting Scene");

    public static void LoadGame() => SceneManager.LoadScene("R1.2.1 Exteriors");

    public static void LoadEndMenu() => SceneManager.LoadScene("Ending Scene");
}
