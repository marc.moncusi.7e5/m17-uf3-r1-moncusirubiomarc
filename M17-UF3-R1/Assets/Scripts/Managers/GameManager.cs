using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    private bool _victory = false;
    public bool Victory { get => _victory; }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Play()
    {
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        ScenesManager.LoadGame();
    }
    public void StartingScene()
    {
        ScenesManager.LoadStartMenu();
    }

    public void GameOver(bool Victory)
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        _victory = Victory;
        ScenesManager.LoadEndMenu();
    }
}
