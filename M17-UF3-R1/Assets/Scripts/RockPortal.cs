using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockPortal : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<InventoryController>().ContainsKeyItem())
        {
            GameManager.Instance.GameOver(true);
        }
    }
}
