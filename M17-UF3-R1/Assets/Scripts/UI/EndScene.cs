using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScene : MonoBehaviour
{
    public Button _button;
    public Text _text;

    void Start()
    {
        Button btn = _button.GetComponent<Button>();
        if (GameManager.Instance.Victory)
        {
            _text.text = "Has guanyat";
        }
        else
        {
            _text.text = "Has perdut";
        }
        btn.onClick.AddListener(TaskOnClick);
    }

    void TaskOnClick()
    {
        GameManager.Instance.StartingScene();
    }
}