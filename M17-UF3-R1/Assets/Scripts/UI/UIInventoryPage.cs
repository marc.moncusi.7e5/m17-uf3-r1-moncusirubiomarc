using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventoryPage : MonoBehaviour
{
    [SerializeField] UIInventoryItem _itemPrefab;
    [SerializeField] RectTransform _contentPanel;

    List<UIInventoryItem> _itemsList = new List<UIInventoryItem>();

    public void InitializeInventoryUI(int inventorysize)
    {
        for (int i = 0; i < inventorysize; i++)
        {
            UIInventoryItem uiItem =
                Instantiate(_itemPrefab, Vector3.zero, Quaternion.identity);
            uiItem.transform.SetParent(_contentPanel);
            _itemsList.Add(uiItem);
        }
    }

    public void UpdateData(int itemIndex,
        Sprite itemImage)
    {
        if (_itemsList.Count > itemIndex)
        {
            _itemsList[itemIndex].SetData(itemImage);
        }
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
