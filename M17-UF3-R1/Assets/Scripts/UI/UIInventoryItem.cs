using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryItem : MonoBehaviour
{
    [SerializeField] Image _itemImage;

    public void Awake()
    {
        ResetData();
    }

    public void ResetData()
    {
        _itemImage.gameObject.SetActive(false);
    }

    public void SetData(Sprite sprite)
    {
        _itemImage.gameObject.SetActive(true);
        _itemImage.sprite = sprite;
    }
}
