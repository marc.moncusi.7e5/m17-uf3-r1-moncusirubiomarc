using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    [SerializeField] UIInventoryPage _inventory;
    [SerializeField] InventorySO _inventoryData;
    [SerializeField] ItemSO _keyItem;

    private void Start()
    {
        PrepareUI();
        _inventoryData.Initialize();
    }

    private void PrepareUI()
    {
        _inventory.InitializeInventoryUI(_inventoryData.Size);
    }

    public void Display()
    {
        if (!_inventory.isActiveAndEnabled)
        {
            _inventory.Show();
            foreach (var item in _inventoryData.GetCurrentInventoryState())
            {
                _inventory.UpdateData(item.Key, item.Value.item.ItemImage);
            }
        }
        else
        {
            _inventory.Hide();
        }
    }
    public void AddItem(ItemSO item)
    {
        if(item != null)
        {
            _inventoryData.AddItem(item);
        }
    }
    public bool ContainsKeyItem()
    {
        foreach (var item in _inventoryData.GetCurrentInventoryState())
        {
            if(item.Value.item == _keyItem)
            {
                return true;
            }
        }
        return false;
    }
}
