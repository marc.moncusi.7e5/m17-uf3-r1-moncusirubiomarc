using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharacterInputController : MonoBehaviour
{
    PlayerInput _playerInput;
    CharacterController _characterController;
    InventoryController _inventoryController;

    #region Variables: Animator Parameters
    Animator _animator;
    int _velocityXHash;
    int _velocityZHash;
    int _crouchedHash;
    int _jumpingHash;
    int _celebratingHash;
    #endregion

    #region Variables: Movement Variables
    Vector3 _appliedMovement;
    Vector3 _currentMovement;
    Vector3 _inputRelativeMovement;
    #endregion

    #region Variables: Input Variables
    Vector2 _currentMovementInput;
    bool _isRunPressed;
    bool _isCrouchPressed;
    float _isAimingPressed;
    bool _isJumpPressed = false;
    float _initialJumpVelocity;
    bool _isJumping = false;
    #endregion

    #region Variables: Constants
    float _rotationFactorPerFrame = 15.0f;
    float _maxJumpHeight = 2.5f;
    float _maxJumpTime = 1.0f;
    float _walkSpeed = 1.0f;
    float _runSpeed = 2.0f;
    float _gravity = -9.8f;
    float _groundedGravity = -0.05f;
    float _zero = 0.0f;
    #endregion

    void Awake()
    {
        playerInputInitialize();
        setupJumpVariables();
        _characterController = GetComponent<CharacterController>();
        _inventoryController = GetComponent<InventoryController>();
        setupAnimator();
    }
    void setupJumpVariables()
    {
        float timeToApex = _maxJumpTime / 2;
        _gravity = (-2 * _maxJumpHeight) / Mathf.Pow(timeToApex, 2);
        _initialJumpVelocity = (2 * _maxJumpHeight) / timeToApex;
    }
    void setupAnimator()
    {
        _animator = GetComponent<Animator>();
        _velocityXHash = Animator.StringToHash("Velocity X");
        _velocityZHash = Animator.StringToHash("Velocity Z");
        _crouchedHash = Animator.StringToHash("isCrouching");
        _jumpingHash = Animator.StringToHash("isJumping");
        _celebratingHash = Animator.StringToHash("isCelebrating");

    }

    void playerInputInitialize()
    {
        _playerInput = new PlayerInput();

        _playerInput.CharacterControls.Move.started += onMove;
        _playerInput.CharacterControls.Move.canceled += onMove;
        _playerInput.CharacterControls.Move.performed += onMove;
        _playerInput.CharacterControls.Run.started += onRun;
        _playerInput.CharacterControls.Run.canceled += onRun;
        _playerInput.CharacterControls.Crouch.started += onCrouch;
        _playerInput.CharacterControls.Crouch.canceled += onCrouch;
        _playerInput.CharacterControls.Jump.started += onJump;
        _playerInput.CharacterControls.Jump.canceled += onJump;
        _playerInput.CharacterControls.Aiming.started += onAiming;
        _playerInput.CharacterControls.Aiming.canceled += onAiming;
        _playerInput.CharacterControls.Shoot.started += onShoot;
        // _playerInput.CharacterControls.Shoot.canceled += onShoot;
        _playerInput.CharacterControls.Celebrate.started += onCelebrate;
        // _playerInput.CharacterControls.Celebrate.canceled += onCelebrate;
        _playerInput.CharacterControls.Interact.started += onInteraction;
        _playerInput.CharacterControls.Interact.canceled += onInteraction;
        _playerInput.CharacterControls.MenuShowing.started += onMenu;
    }

    void onMove(InputAction.CallbackContext context)
    {
        _currentMovementInput = context.ReadValue<Vector2>();
        _currentMovement.x = _currentMovementInput.x;
        _currentMovement.z = _currentMovementInput.y;
    }

    void onRun(InputAction.CallbackContext context)
    {
        _isRunPressed = context.ReadValueAsButton();
    }

    void onCrouch(InputAction.CallbackContext context)
    {
        _isCrouchPressed = context.ReadValueAsButton();
    }

    void onJump(InputAction.CallbackContext context)
    {
        _isJumpPressed = context.ReadValueAsButton();
    }

    void onAiming(InputAction.CallbackContext context)
    {
        _isAimingPressed = context.ReadValue<float>();
    }

    void onShoot(InputAction.CallbackContext context)
    {
        
    }

    void onCelebrate(InputAction.CallbackContext context)
    {
        _animator.SetTrigger(_celebratingHash);
        StartCoroutine(WaitForDance());
    }

    void onInteraction(InputAction.CallbackContext context)
    {
    }

    void onMenu(InputAction.CallbackContext context)
    {
        _inventoryController.Display();
    }

    private IEnumerator WaitForDance()
    {
        OnDisable();
        yield return new WaitForSeconds(2f);//Wait for the transition
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        OnEnable();
    }

    void OnEnable()
    {
        _playerInput.CharacterControls.Enable();
    }

    void OnDisable()
    {
        _playerInput.CharacterControls.Disable();
    }

    void Update()
    { 
        HandleMovement();
        HandleRotation();
        HandleGravity();
        HandleJump();
        HandleAnimation();
        _characterController.Move(_appliedMovement * Time.deltaTime);
    }

    void HandleAnimation()
    {
        _animator.SetFloat(_velocityXHash, _inputRelativeMovement.x);
        _animator.SetFloat(_velocityZHash, _inputRelativeMovement.z);
        _animator.SetBool(_crouchedHash, _isCrouchPressed);
        _animator.SetBool(_jumpingHash, _isJumping);
        _animator.SetLayerWeight(1, _isAimingPressed);
    }

    void HandleMovement()
    {

        if (_isRunPressed && !_isCrouchPressed)
        {
            _inputRelativeMovement.x = _currentMovement.x * _runSpeed;
            _inputRelativeMovement.z = _currentMovement.z * _runSpeed;
        }
        else
        {
            _inputRelativeMovement.x = _currentMovement.x * _walkSpeed;
            _inputRelativeMovement.z = _currentMovement.z * _walkSpeed;
        }
        _appliedMovement = ConvertToCameraSpace(_inputRelativeMovement);
        
    }

    void HandleJump()
    {
        if (!_isJumping && _characterController.isGrounded && _isJumpPressed)
        {
            _isJumping = true;
            _currentMovement.y = _initialJumpVelocity;
            _appliedMovement.y = _initialJumpVelocity;
        }
        else if (!_isJumpPressed && _isJumping && _characterController.isGrounded)
        {
            _isJumping = false;
        }
    }

    void HandleGravity()
    {
        if (_characterController.isGrounded)
        {
            _currentMovement.y = _groundedGravity;
            _inputRelativeMovement.y = _groundedGravity;
        }
        else
        {
            float previousYVelocity = _currentMovement.y;
            _currentMovement.y = _currentMovement.y + (_gravity * Time.deltaTime);
            _appliedMovement.y = Mathf.Max((previousYVelocity + _currentMovement.y) * 0.5f, -20f);
        }
    }

    Vector3 ConvertToCameraSpace(Vector3 vectorToRotate)
    {
        float YValue = vectorToRotate.y;
        Vector3 cameraForward = Camera.main.transform.forward;
        Vector3 cameraRight = Camera.main.transform.right;

        cameraForward.y = 0;
        cameraRight.y = 0;

        cameraForward = cameraForward.normalized;
        cameraRight = cameraRight.normalized;

        Vector3 cameraForwardZProduct = vectorToRotate.z * cameraForward;
        Vector3 cameraRightXProduct = vectorToRotate.x * cameraRight;

        Vector3 vectorRotated = cameraForwardZProduct + cameraRightXProduct;
        vectorRotated.y = YValue;
        return vectorRotated;
    }

    void HandleRotation()
    {
        Vector3 positionToLookAt;

        positionToLookAt = Camera.main.transform.forward;
        positionToLookAt.y = _zero;

        Quaternion currentRotation = transform.rotation;

        Quaternion targetRotation = Quaternion.LookRotation(positionToLookAt);
        transform.rotation = Quaternion.Slerp(currentRotation, targetRotation, _rotationFactorPerFrame * Time.deltaTime);
    }
}
