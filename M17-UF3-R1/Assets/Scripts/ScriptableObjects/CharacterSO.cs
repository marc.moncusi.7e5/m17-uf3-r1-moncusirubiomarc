using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCharacter", menuName = "ScriptableObjects/Characters")]
public class CharacterSO : ScriptableObject
{
    [Header("Character Stats")]
    public float maxHealth;
    public float currentHealth;
}
