using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IDamagable
{
    [SerializeField] public CharacterSO _stats;

    public float damage { get; set; }

    void Awake()
    {
        _stats = ScriptableObject.Instantiate(_stats);
    }

    public void ApplyDamage(float damageTaken)
    {
        _stats.currentHealth -= damageTaken;
        Debug.Log(_stats.currentHealth);
        if (_stats.currentHealth <= 0) onDie();
    }


    public void onDie()
    {
        GameManager.Instance.GameOver(false);
    }
}
