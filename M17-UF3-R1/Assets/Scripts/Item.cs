using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour, IInteractable
{
    [SerializeField] ItemSO _itemData;
    [SerializeField] AudioSource _audioSource;

    public IEnumerator Interact()
    {
        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        Destroy(this.gameObject);
    }


    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(Interact());
        this.gameObject.GetComponent<Collider>().enabled = false;
        other.gameObject.GetComponent<InventoryController>().AddItem(_itemData);
    }

}
